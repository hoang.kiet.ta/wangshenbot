import os
import asyncio

import discord
import youtube_dl
from discord.ext import commands



# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': 'songs/%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.song_list = []
        self.cursor = 0
        self.channel = None

        self.bot.loop.create_task(self.audio_player_task())

    @commands.command()
    async def join(self, ctx, *, channel: discord.VoiceChannel):

        if ctx.voice_client is not None:
            return await ctx.voice_client.move_to(channel)

        await channel.connect()

    @commands.command()
    async def play(self, ctx, *, url):

        player = await YTDLSource.from_url(url, loop=self.bot.loop)
        await songs.put(player)
        self.song_list.append(player.title)

    @commands.command()
    async def pause(self, ctx):

        self.channel.pause()
        await ctx.send("Music bot has been paused!")

    @commands.command()
    async def resume(self, ctx):

        self.channel.resume()
        await ctx.send("Music bot has been resumed!")

    @commands.command()
    async def next(self, ctx):
        pass

    @commands.command()
    async def previous(self, ctx):
        pass

    @commands.command()
    async def queue(self, ctx):

        queue_string = 'Queue:\n'

        for i, s in enumerate(self.song_list):
            queue_string += s

            if i == self.cursor:
                queue_string += ' [Current Song]'

            queue_string += '\n'
        await ctx.send(queue_string)

    @commands.command()
    async def song(self, ctx):
        await ctx.send("Current Song: " + self.channel.source.title)

    @commands.command()
    async def volume(self, ctx, volume: int):

        if ctx.voice_client is None:
            return await ctx.send("Not connected to a voice channel.")

        ctx.voice_client.source.volume = volume / 100
        await ctx.send("Changed volume to {}%".format(volume))

    @commands.command()
    async def leave(self, ctx):

        await ctx.voice_client.disconnect()

    @play.before_invoke
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                self.channel = await ctx.author.voice.channel.connect()
            else:
                await ctx.send("You are not connected to a voice channel.")
                raise commands.CommandError("Author not connected to a voice channel.")
        elif ctx.voice_client.is_playing():
            await ctx.send("Song is enqueued!")

    async def audio_player_task(self):
        while True:
            play_next_song.clear()
            current = await songs.get()
            print(current.title)
            self.channel.play(current, after=self.toggle_next)
            await play_next_song.wait()

    def toggle_next(self, e):
        try:
            self.cursor += 1
            self.bot.loop.call_soon_threadsafe(play_next_song.set)
        except e:
            self.channel.stop()


songs = asyncio.Queue()
play_next_song = asyncio.Event()


def setup(bot):
    bot.add_cog(Music(bot))
